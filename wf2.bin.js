#!/usr/bin/env node

'use strict';

const path = require('path');
const fs = require('fs');
const tmp = require('tmp');
const spawn = require('child_process').spawn;

const appDir = process.cwd();
const binDir = path.join(appDir, 'node_modules', '.bin');

const appConfig = require(path.join(appDir, 'config.js'));

const args = process.argv.slice(2);

// Create the tmp config file for sequelize migrations
function sequelizeTmpFile() {
    let tmpFile = tmp.fileSync({
        dir: appDir,
        prefix: 'sql-config-',
        postfix: '.json'
    });

    let data = JSON.stringify(appConfig.database, null, 2);
    fs.writeFileSync(tmpFile.name, data, 'utf8');

    return tmpFile;
}

function exec(cmdName, argArray, cleanup, error) {
    let cmd = spawn(cmdName, argArray);

    cmd.on('error', error);
    cmd.on('close', cleanup);

    cmd.stdout.on('data', d => {
        console.log(d.toString('utf8'));
    });

    cmd.stderr.on('data', d => {
        console.log(d.toString('utf8'));
    });
}

if(args[0] === 'start') {
    const App = require('./index');
    App.Start();
}

if(args[0].indexOf('migration:') === 0) {
    let sqlTmpFile = sequelizeTmpFile();
    switch(args[0]) {
        case 'migration:create':
            exec(
                    path.join(binDir, 'sequelize'),
                    [
                    'migration:create',
                    '--config',
                    sqlTmpFile.name
                    ],
                    () => {
                        sqlTmpFile.removeCallback();
                    },
                    (e) => {
                        console.log(e);
                    }
                );
            break;

        case 'migration:run':
            exec(
                    path.join(binDir, 'sequelize'),
                    [
                    'db:migrate',
                    '--config',
                    sqlTmpFile.name
                    ],
                    () => {
                        sqlTmpFile.removeCallback();
                    },
                    (e) => {
                        console.log(e);
                    }
                );
            break;

        case 'migration:undo':
            exec(
                    path.join(binDir, 'sequelize'),
                    [
                    'db:migrate:undo',
                    '--config',
                    sqlTmpFile.name
                    ],
                    () => {
                        sqlTmpFile.removeCallback();
                    },
                    (e) => {
                        console.log(e);
                    }
                );
            break;
    }
}
