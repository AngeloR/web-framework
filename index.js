'use strict';

const path = require('path');
global.Dir = {
    App: process.cwd(),
    Fw: __dirname
}

global.AppConfig = require(path.join(global.Dir.App, 'config.js'));

const App = require(path.join(global.Dir.Fw, 'core'));

const server = new App.Server({
    modelDir: path.join(global.Dir.App, 'models'),
    controllerDir: path.join(global.Dir.App, 'controllers'),
    http: global.AppConfig.http
});


function diag() {
    let lines = [];

    lines.push('HTTP Server:', global.AppConfig.http.port);

    console.log(lines.join("\n"));
}

App.Start = function(fn) {
    server.start().then(() => {
        fn ? fn() : diag();
    }).catch((e) => {
        console.log(e);
    });
}

module.exports = App;
