'use strict';

const qs = require('querystring');
const url = require('url');
const path = require('path');
const RequestBase = require(path.join(__dirname, '..', 'request-base'));

class HttpRequest extends RequestBase {
    constructor(raw) {
        super(raw);
    }

    handleRequest() {
        this.method = this.raw.method.toUpperCase();
        this.url = url.parse(this.raw.url);
        this.querystring = qs.parse(this.url.query);
        this.headers = this.raw.headers;

        this.body = '';

        this.raw.on('data', chunk => {
            this.body += chunk;
        });

        this.raw.on('end', () => {
            try {
                this.body = JSON.parse(this.body);
            }
            catch(e) {
            }
            this.emit('ready', this);
        });
    }
}

module.exports = HttpRequest;
