'use strict';

const path = require('path');
const ResponseBase = require(path.join(__dirname, '..', 'response-base'));

class HttpResponse extends ResponseBase {
    constructor(raw) {
        super(raw);

        this.raw = raw;

    }

    setHeader(key, value) {
        this.raw.setHeader(key, value);
    }

    setHeaders(obj) {
        Object.keys(obj).forEach((key) => {
            this.raw.setHeader(key, obj[key]);
        });
    }

    send(jsonResponse) {
        this.raw.statusCode = jsonResponse.meta.statusCode;
        this.setHeader('Content-type', 'application/json');
        this.raw.end(JSON.stringify(jsonResponse));
    }
}

module.exports = HttpResponse;
