'use strict';

const http = require('http');
const Promise = require('bluebird');
const path = require('path');
const TransportBase = require(path.join(__dirname, '..', 'transport-base'));

const HttpRequest = require('./request');
const HttpResponse = require('./response');

class Http extends TransportBase {
    constructor(opts) {
        super(opts);

        this.server = http.createServer(this.handler.bind(this));
    }

    start() {
        const port = this.options.port ? this.options.port : 3000;
        return new Promise((resolve, reject) => {
            this.server.listen(port, (err) => {
                if(err) {
                    reject(err);
                    return;
                }

                resolve();
                return;
            });
        });
    }

    handler(rawRequest, rawResponse) {
        const req = new HttpRequest(rawRequest);
        const res = new HttpResponse(rawResponse);

        req.on('ready', (req) => {
            this.emit('request', req, res);
        });
    }
}

module.exports = Http;
