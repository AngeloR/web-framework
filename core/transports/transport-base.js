'use strict';

const EventEmitter = require('events').EventEmitter;

class TransportBase extends EventEmitter {
    constructor(opts) {
        super();

        this.options = opts;
    }

    start() {
        throw new Error('Please configure a start() method for this transport');
    }
}

module.exports = TransportBase;
