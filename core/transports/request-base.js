'use strict';

const EventEmitter = require('events').EventEmitter;

class RequestBase extends EventEmitter {
    constructor(raw) {
        super();
        this.raw = raw;

        this.handleRequest();
    }

    handleRequest() {
        throw new Error('Please override handleRequest');
    }
}

module.exports = RequestBase;
