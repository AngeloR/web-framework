'use strict';

class ResponseBase {
    constructor() {
    }

    defaultResponse(errors, data) {
        let response = {
            meta: {},
            errors: [],
            body: data || {}
        };

        if(errors)
            response.errors.push(errors);

        return response;
    }

    send() {
        throw new Error('Please override the `send` method for this transport');
    }

    ok(data) {
        let response = this.defaultResponse(null, data);
        response.meta.statusCode = 200;

        this.send(response);
    }

    badRequest(message) {
        let response = this.defaultResponse(data);
        response.meta.statusCode = 400;

        this.send(response);
    }

    internalError(data) {
        let response = this.defaultResponse(data);
        response.meta.statusCode = 500;

        this.send(response);
    }

    notFound(data) {
        let response = this.defaultResponse(null, data);
        response.meta.statusCode = 404;

        this.send(response);
    }

    forbidden(data) {
        let response = this.defaultResponse(null, data);
        response.meta.statusCode = 403;

        this.send(response);
    }

    notImplemented() {
        let response = this.defaultResponse('Not implemented');
        response.meta.statusCode = 501;

        this.send(response);
    }
}

module.exports = ResponseBase;
