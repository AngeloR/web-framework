'use strict';

const assert = require('assert');
const sequelizeInstance = require('./orm/sequelize');
const Sequelize = require('sequelize');
const Promise = require('bluebird');

class BaseModel {

    /*
     * Ensures model conforms to spec
     *  if it does,     it will call `createSchema`
     *  if it does not, it will throw an error
     *
     * @param args <array>
     *      args[0] should be bookshelf
     *
     *
     * Validates existance of
     *
     * methods: 
     *  - createSchema
     */
    constructor(args) {
        this._name = this.constructor.name;

        this.sequelize = sequelizeInstance;

        // @reference http://docs.sequelizejs.com/en/v3/docs/models-definition/#data-types
        this.Type = Sequelize.DataTypes;

        // This is where the child object can define the actual fields 
        // that belong to this model
        this.fields = {};

        // After the user defined `definition` method is called, this is set 
        // to the actual model, until that time it is emtpy
        this.modelName = '';
        this.model = undefined;

        this.validateChildImplementation();

        this.configureModel();
    }

    /*
     * This ensures that the child model properly implements the required 
     * interface, given that we don't actually get interfaces in this 
     * version of JS
     */
    validateChildImplementation() {
        assert.equal(typeof this.definition, 'function', 'Please override the definition method for the ['+this._name+'] model');
    }

    field(name, definition) {
        this.fields[name] = definition;
    }

    /*
     * Uses the user defined "definition" method to create the sequelize model 
     * that is used for data access/modification
     */
    configureModel() {
        this.definition();
        
        assert(this.modelName.length);

        this.model = this.sequelize.define(this.modelName, this.fields);

        if(typeof this.relations === 'function') {
            this.relations();
        }
    } 

    /*
     * Create a new instance of the model and return it
     */
    create(properties) {
        // ensure we create a new instance of the model
        let modelInstance = new this.constructor();

        return new Promise((resolve, reject) => {
            let build = modelInstance.model.build(properties);
            build.validate().then(() => {
                build.save().then(resolve).catch(reject);
            }).catch(reject);

        });
    }

    /*
     * List/query the database utilizing the filter for restricting the 
     * data returned. 
     *
     * This returns a RAW model as defined by sequelize.
     */
    list(filter) {
        return this.model.findAll(filter ? filter : {});
    }

    find(filter) {
        return this.model.findOne(filter ? filter : {});
    }

    create(props) {
        return this.model.build(props).save();
    }

    /*
     * Relation map
     */
    hasMany(modelInstance, definition) {
        this.model.hasMany(modelInstance.model, definition);
    }
}

module.exports = BaseModel;
