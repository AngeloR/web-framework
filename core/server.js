'use strict';

const path = require('path');
const Promise = require('bluebird');
const EventEmitter = require('events').EventEmitter;
const Http = require('./transports/http');
const Router = require('./router');

class Server extends EventEmitter {
    constructor(opts) {
        super();

        this.options = opts;

        this.router = new Router();

        this.configureHttp();
        this.configureWebsocket();
    }

    configureHttp() {
        this.http = new Http(this.options.http ? this.options.http : {});

        this.http.on('error', (err) => {
            this.emit('error', err);
            return;
        });

        this.http.on('ready', () => {
            this.emit('ready');
        });

        this.http.on('request', (req, res) => {
            this.handleRequest(req, res);
        });
    }

    configureWebsocket() {
        // @TODO
    }

    handleRequest(req, res) {
        req.params = {};
        let invokeMethod = 'index';
        this.router.parse(req.url.pathname).forEach((modelInterface) => {
            if(modelInterface[1]) {
                req.params[modelInterface[0] + 'Id'] = modelInterface[1];
                switch(req.method) {
                    case 'GET':
                        invokeMethod = 'get';
                        break;
                    case 'POST':
                        invokeMethod = 'update';
                        break;
                    case 'DELETE':
                        invokeMethod = 'delete';
                        break;
                    default:
                        invokeMethod = 'index';
                        break;
                }
            }
            else {
                switch(req.method) {
                    case 'GET':
                        invokeMethod = 'index';
                        break;
                    case 'POST':
                        invokeMethod = 'create';
                        break;
                    default: 
                        invokeMethod: 'index';
                }
            }
        });

        let models = this.loadModels(req.url.pathname);
        let controller = this.loadController(req.url.pathname);

        Object.keys(models).forEach((key) => {
            controller.Model[key] = models[key];
        });

        if(!controller) {
            return res.internalError();
        }

        if(typeof controller[invokeMethod] !== 'function') {
            return res.notImplemented();
        }

        controller[invokeMethod](req, res);
    }

    loadController(urlPath) {
        try {
            let controllerName = this.router.parse(urlPath).pop()[0];

            let controllerPath = path.join(this.options.controllerDir, controllerName);

            let Controller = require(controllerPath);
            let controller = new Controller();

            return controller;
        }
        catch(e) {
            return null;
        }
    }
    
    loadModels(urlPath) {
        let models = {};

        try {
            this.router.parse(urlPath).forEach((modelInterface) => {
                let model = this.loadModel(modelInterface[0]);
                if(model) {
                    models[model.modelName] = model;
                }
            });
        }
        catch(e) {
        }

        return models;
    }

    loadModel(name) {
        try {
            let filePath = path.join(this.options.modelDir, name);
            let model = require(filePath);
            return new model();
        }
        catch(e) {
            console.log(e);
            return;
        }
    }

    start() {
        return Promise.all([
            this.http.start()
        ]);
    }
}

module.exports = Server;
