'use strict';

const Sequelize = require('sequelize');
const sequelize = new Sequelize(global.AppConfig.database);

module.exports = sequelize;
