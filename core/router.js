'use strict';

const EventEmitter = require('events').EventEmitter;

class Router extends EventEmitter {
    constructor() {
        super();
    }

    chunk(array) {
        let parsedChunks = [];
        while(array.length) {
            parsedChunks.push(array.splice(0, 2));
        }

        return parsedChunks;
    }

    parse(url) {
        let urlPieces = url.split('/');
        urlPieces.shift();
        // At this point we have a multi-dimensional
        // array that follows the format
        // [ [controller, id], [controller, id] ]
        // Where the last ID in a sequence be present
        let chunks = this.chunk(urlPieces);

        return chunks;
    }
}

module.exports = Router;
