# What is this?
This is some glue code that gives you
1. An model/controller interface for writing restful node.js APIs
2. A db migration system utilizing Sequelize

It's really the base of 75% of the applications you'll need to build.

This framework is still very much in development and as such I would caution 
against using it in production. 

This framework is still yet to be named. I'm terrible at thinking up names

# Proposed Usage

## `npm install --save framework`
Just install the framework into whatever project you're currently working on. 
Whether or not you have an existing application is mostly irrelevant. This is 
designed to work alongside your existing application allowing you to slowly 
transfer application logic from your existing system to this one.

It does, however, require the following folders/files

```
controllers/
models/
migrations/ (auto created)
config.js
```

You can have empty models and controllers directories.. and the migrations 
folder will actually be auto-created. The config.js file is really the only 
HARD requirement. This project has a sample configuration file that you can 
use as your base. 

# Application Development
Simply create a controller. That controller is automatically made available 
at the '/[name]' where [name] is the name of your controller. Routes are auto 
mapping to different functions within that controller.

For example, given the following controllers:
```
class UserController;
class TodoController;
```

We will end up automapping routes like this  
- GET    /user           = UserController.list();
- GET    /user/:id       = UserController.get();
- POST   /user           = UserController.create();
- POST   /user/:id       = UserController.update();
- DELETE /user/:id       = UserController.delete();
- GET    /user/:id/todo  = TodoController.list();
- GET    /todo           = TodoController.list()
- POST   /todo/:id       = TodoController.get();

Each controller has access to the model via `this.Model.[model_name]` syntax. 
So for `/user` your model would be available as `this.Model.user`. 

Models are lazy-loaded based on the routes hit.

## Starting the Server
You can easily start a new server by typing `./node_modules/.bin/wf2 start`

# Migrations
## `./node_modules/.bin/wf2 migration:create`
This will create a new migration file in the `migrations/` folder with the 
proper scaffolding for `up/down` You can find further documentation on how 
migrations work here: [Sequelize Migrations](http://docs.sequelizejs.com/manual/tutorial/migrations.html).

## `./node_modules/.bin/wf2 migration:run`
This will run all pending migrations

## `./node_modules/.bin/wf2 migration:undo`
This will rollback the latest migration run

*Note*: This project aims to tackle the majority of applications. It will NOT help you 
in every case. It is designed to help people building ReSTful APIs for their 
applications. It provides tooling and convention that is similar to frameworks 
in other languages.
